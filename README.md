## Raneto Docker image ##

Autobuilded `raneto` image base on up-to-date `debian:jessie` (x64 architecture).

### Docker tags & Git Branch ###
- `latest` => `0.11.0` / branch master 
- `0.11.0` / branch 0.11.0 

### Image build & optimization ###

This image is "one layer image", all `RUN` commands was group into one in order to reduce the final image size. 
Moreover, every `ADD` or `COPY` commands point on an external URL instead of local files. So if you want to rebuild the image, it's not necessary to pull any git repository before, you could just do:

    docker build -t "raneto:latest" https://bitbucket.org/jtof_fap/raneto/raw/master/Dockerfile

The `Raneto` listening port and version are an argument (ARG) in Dockerfile, so you could choose a specific raneto version on the fly in build process.

    docker build -t "raneto:0.11.0" --build-arg VERSION=0.11.0 https://bitbucket.org/jtof_fap/raneto/raw/master/Dockerfile

Version numbers are available here: [https://github.com/gilbitron/Raneto/tags](https://github.com/gilbitron/Raneto/tags)

### Image automatic updates ###

This image is automatically rebuilt when: 

- When base image is updated: 

This image is based on a `node:6.4.0` image based on an up-to-date debian jessie (`insecurity/node:6.4.0`) and is automatically rebuild when base (or NodeJS) image is updated. For more information about this process, reports to: 
[https://hub.docker.com/r/insecurity/debian/](https://hub.docker.com/r/insecurity/debian/) 

- When git repository is updated ([https://bitbucket.org/jtof_fap/raneto/](https://bitbucket.org/jtof_fap/raneto/)):

### Getting this image ###

    docker pull insecurity/raneto:latest
    docker pull insecurity/raneto:0.11.0

### Quick run this image ###

Using Git:

    git clone https://jtof_fap@bitbucket.org/jtof_fap/raneto.git && cd raneto
    mkdir -p ./Data/Content/ ./Data/Conf ./Data/Theme && chown -R 1000:1000 ./Data && chmod -R 700 ./Data
    cp Compose/docker-compose.yml ./
    docker-compose up -d

Without using Git:

    mkdir Raneto && cd Raneto
    mkdir -p ./Data/Content/ ./Data/Conf ./Data/Theme && chown -R 1000:1000 ./Data && chmod -R 700 ./Data
    wget https://bitbucket.org/jtof_fap/raneto/raw/master/Dockerfile
    wget https://bitbucket.org/jtof_fap/raneto/raw/master/Compose/docker-compose.yml
    docker-compose up -d
    
### Persistence configuration ###

To run this image, you could use the following `docker-compose.yml` file: 

- [https://bitbucket.org/jtof_fap/raneto/raw/master/Compose/docker-compose.yml](https://bitbucket.org/jtof_fap/raneto/raw/master/Compose/docker-compose.yml) 

This container use 3 volumes :

    $DATADIR/Conf:/srv/raneto/conf => CONFIGURATION File
    $DATADIR/Content:srv/raneto/content => Mardown (.md) directory
    $DATADIR/Theme:/srv/raneto/themes/custom   => Theme for customization

The first `docker-compose up` will copy (from run.sh) in volumes the default **theme** directory in **$DATADIR/Theme** copy the configuration file `config.default.js` in **$DATADIR/Conf**. At this step, **$DATADIR/Content** is still empty and raneto shows the `/srv/raneto/example/content` directory in container.

To custom your install, you must edit the `$DATADIR/Conf/config.default.js` file and change the line:

    content_dir : __dirname + '/../example/content/',

By:

    content_dir : __dirname + '/../content/',

Now you can populate your `$DATADIR/Content` directory with your `markdown` files and restart the container.

### Container security ###

In order to limit the security impacts in case of service compromission, this container is a `non-root-user-container`.

- **Container runs with `raneto` user**

To do this, some changes are required from other `raneto` images. **Non-root** user **can't bind ports < 1024** so default port (EXPOSE) stay to `3000`. The **Dockerfile** affects also 1000:1000 `UID/GID` to raneto user and `run.sh` change all permissions for this user in local volumes.

- **Capabilities are limited in docker-compose file**:

Moreover, in `docker-compose.yml` file, all capabilities allowed by default on docker container are dropped and some security options are enhanced:

    cap_drop:
      - ALL

    security_opt:
      - no-new-privileges




